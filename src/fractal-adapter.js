'use strict';

const fs = require('fs');
const path = require('path');
const {execSync} = require('child_process');
const {Adapter} = require('@frctl/fractal');
const VueServerRenderer = require('@vue/server-renderer');
const VueFilesHelper = require('./helper/VueFilesHelper');
const Service = require('@vue/cli-service/lib/Service');

const projectDir = path.join(__dirname, '../../../..');
const projectDist = path.join(projectDir, 'dist');

class FractalVueAdapter extends Adapter {
    constructor(source, app, config) {
        super(null, source);

        this._app = app;
        this._config = config;
        this._appConfig = Object.assign({}, this._app.config(), {docs: null});
        this._rebuilding = false;
        this._rebuildingSetTimeout = null;

        this.reloadVueApp();

        // As soon a source changes, the vue component definition needs to be updated
        source.on('updated', this.rebuildVueApp.bind(this));
    }

    reloadVueApp() {
        const manifest = require(path.join(projectDist, 'server/ssr-manifest.json'));
        this._VueApp = require(
            path.join(projectDist, 'server', manifest['app.js'])
        ).default;

        // client/index.html moved to allow fratomic create own index file for static build
        const tplFile = path.join(projectDist, 'client/_index.html');
        if (fs.existsSync(path.join(projectDist, 'client/index.html'))) {
            fs.renameSync(path.join(projectDist, 'client/index.html'), tplFile);
        }

        this._template = fs.readFileSync(tplFile, 'utf8');
    }

    render(renderPath, str, props, meta) {
        const compName = 'Page' + VueFilesHelper.fileNameToComponentName(renderPath);

        let contextStore = null;
        if (props && props._store) {
            contextStore = props._store;
            delete props._store;
        }

        const appContext = {
            url: '/fractal-page-' + compName,
            componentName: compName,
            props: props,
        };

        const globalStore = require(
            path.join(this._appConfig.components.path, 'alivue.config.json')
        ).store || null;

        const useTemplate = !meta.env.request.path.startsWith('/components/detail');

        return this._VueApp(appContext).then((app) => {
            return VueServerRenderer.renderToString(app).then(html => {
                if (!useTemplate) {
                    return html;
                }

                let initialState = appContext.rendered();
                if (globalStore) {
                    initialState = this.mergeDeep(initialState, globalStore);
                }
                if (contextStore) {
                    initialState = this.mergeDeep(initialState, contextStore);
                }

                const script = initialState
                    ? `<script>window.__INITIAL_STATE__=${JSON.stringify(initialState)}</script>`
                    : '';

                return this._template
                    .replace(/<div id="app"([^>]+?)>/, '<div id="app"$1>' + html)
                    .replace(/<body([^>]+?)>/, '<body$1>' + script);
            }).catch(err => {
                console.error(err);
                return err;
            });
        }, (err) => {
            console.error(err);
        });
    }

    rebuildVueApp(event) {
        // make sure it's not running >1 vue builds at the same time
        if (this._rebuilding !== false) {
            if (!this._rebuildingSetTimeout) {
                this._rebuildingSetTimeout = setTimeout(() => {
                    clearTimeout(this._rebuildingSetTimeout);
                    this._rebuildingSetTimeout = null;
                    this.rebuildVueApp(event);
                }, 3000);
            }
            return;
        }
        this._rebuilding = true;

        console.log('source code updated, rebuilding...');
        execSync('fractal alivue-build -p');

        const rebuildServer = () => {
            // async build of both may take too much memory, and development of server-side is not working
            process.env.VUE_ENV = 'server';
            const vueCliServer = new Service(process.cwd());
            vueCliServer.init('production');
            vueCliServer.projectOptions.outputDir = 'dist/server';

            vueCliServer.run('build', {'mode': 'production'})
                .then(() => {
                    this.clearRequireCache();
                    this.reloadVueApp();
                    this._app.emit('source:updated', 'prototype', {});
                    this._rebuilding = false;
                })
                .catch(err => {
                    console.log(err);
                    this._rebuilding = false;
                });
        };

        // rebuild client + run rebuildServer
        process.env.VUE_ENV = 'client';
        // new Service instance need to be created here, as it impact vueCliServer because of internal vue reasons
        const vueCliClient = new Service(process.cwd());
        vueCliClient.init('development');
        vueCliClient.projectOptions.outputDir = 'dist/client';
        vueCliClient.run('build', {'mode': 'development'})
            .then(rebuildServer)
            .catch(err => {
                console.log(err);
                this._rebuilding = false;
            });
    }

    clearRequireCache() {
        Object.keys(require.cache).forEach(key => {
            if (key.includes('.vue') || key.includes('.json') || key.includes('/js/')) {
                delete require.cache[key];
            }
        });
    }

    isObject(item) {
        return (item && typeof item === 'object' && !Array.isArray(item));
    }

    mergeDeep(target, ...sources) {
        if (!sources.length) {
            return target;
        }
        const source = sources.shift();

        if (this.isObject(target) && this.isObject(source)) {
            for (const key in source) {
                if (this.isObject(source[key])) {
                    if (!target[key]) Object.assign(target, {[key]: {}});
                    this.mergeDeep(target[key], source[key]);
                } else {
                    Object.assign(target, {[key]: source[key]});
                }
            }
        }

        return this.mergeDeep(target, ...sources);
    }
}

module.exports = config => {
    config = config || {};

    return {
        register(source, app) {
            return new FractalVueAdapter(source, app, config);
        }
    };
};
