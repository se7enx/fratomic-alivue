'use strict';

const BuildFratomic = require('./service/BuildFratomic');

class AlivueCli {
    constructor(fractal) {
        this._fractal = fractal;
        this._buildFratomic = new BuildFratomic();
        this.registerCommands(fractal.cli);
    }

    build(args) {
        this._buildFratomic.buildComponents(
            './',
            this._fractal.components,
            args.options.prototype === true
        );
    }

    registerCommands(cli) {
        cli.command('alivue-build', this.build.bind(this), {
            description: 'Build prototype components to project directory',
            options: [
                ['-p, --prototype', 'Specify that build is for fratomic prototype, not a live webiste'],
            ]
        });
    }
}

module.exports = AlivueCli;
