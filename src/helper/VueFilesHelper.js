const VueCompiler = require('@vue/compiler-sfc');
const path = require('path');
const fs = require('fs');

class VueFilesHelper {
    emptyComponent() {
        return '<template><div style="display: none"></div></template>';
    }

    fixImport(content) {
        return content.replace(
            /import[ \t\n\r]+([{} A-z0-9_]+)[ \t\n\r]+from[ \t\n\r]+["']([A-z0-9\/\-._]+?)["']/g,
            function (match, className, importFilePath) {
                const fileName = importFilePath.substring(importFilePath.lastIndexOf('/') + 1);
                if (importFilePath === fileName || importFilePath.startsWith('@')) {
                    // don't touch module import, only relative components
                    return `import ${className} from "${importFilePath}"`;
                }

                return `import ${className} from "./${fileName}"`;
            }
        );
    }

    fixStyleInclude(content) {
        if (content.indexOf('<style ') === -1) {
            return content;
        }

        // <style src="../../../src/scss/file.scss"> => <style src="../scss/file.scss">
        content = content.replace(/<style (.*?)src=["|'](.*?)["|']/g, function (match, attributes, filePath) {
            filePath = filePath.substring(filePath.indexOf('../src/') + 7);

            return `<style ${attributes}src="../../${filePath}"`;
        });

        // @import "../../../src/scss/file.scss" => @import "../../scss/file.scss"
        content = content.replace(/@import\s+?["|'](.*?)["|']/g, function (match, filePath) {
            filePath = filePath.substring(filePath.indexOf('../src/') + 7);

            return `@import "../../${filePath}"`;
        });

        return content;
    }

    generateRoutesDefinitionFile(routes) {
        let imports = '';
        let routesDef = [];

        routes.forEach(route => {
            const compName = 'Page' + this.fileNameToComponentName(route.fileName);

            // route component definition
            let routeComponentDef;
            if (route.lazy) {
                let webpackConfig = '';
                if (route.chunkName) {
                    webpackConfig += `/* webpackChunkName: ${JSON.stringify(route.chunkName)} */ `;
                }
                routeComponentDef = `() => import(${webpackConfig}'./components/${route.fileName}')`;
            } else {
                imports += `import ${compName} from './components/${route.fileName}';\n`;
                routeComponentDef = compName;
            }

            // meta tags
            let meta = [];
            if (route.config.metaTitle) {
                meta.push('title:' + JSON.stringify(route.config.metaTitle));
            }
            if (route.config.metaKeywords) {
                meta.push('keywords:' + JSON.stringify(route.config.metaKeywords));
            }
            if (route.config.metaDescription) {
                meta.push('description:' + JSON.stringify(route.config.metaDescription));
            }
            if (route.config.metaImageUrl) {
                meta.push('imageUrl:' + JSON.stringify(route.config.metaImageUrl));
            }
            const routeName = route.config.name || compName;

            // final vue route definition
            let routeDef = `path: "${route.config.route}"`;
            routeDef += `, component: ${routeComponentDef}`
            routeDef += `, name: "${routeName}"`
            if (meta.length) {
                routeDef += `, meta:{ ${meta.join(',')} }`
            }

            routesDef.push('{' + routeDef + '}');
        });

        // we need empty element for Vue routing
        imports += 'import Empty from "./components/empty";\n';
        routesDef.push(`{path: '/:pathMatch(.*)', component: Empty, name: 'PageEmpty'}`);

        return imports + 'export default [\n' + routesDef.join(',\n') + '\n];';
    }

    generateDynamicPageComponents(pageComponents, componentRootPath, withMock) {
        let compDef = [];
        let compImports = '';
        let compReg = '';
        let mockDef = '';
        if (withMock) {
            mockDef += "\n\nimport RequestHandler from '@/handler/RequestHandler';\n" +
                "import MockAdapter from 'axios-mock-adapter';\n" +
                "const mock = new MockAdapter(RequestHandler.axiosInstance);\n\n" ;
        }

        pageComponents.forEach(comp => {
            const compName = 'Page' + this.fileNameToComponentName(comp.fileName);
            const compPath = path.join(process.cwd(), componentRootPath);

            // detect properties list
            let compProps;
            try {
                compProps = VueCompiler.compileScript(
                    VueCompiler.parse(
                        fs.readFileSync(
                            path.join(compPath, comp.fileName + '.vue'),
                            'utf8'
                        )
                    ).descriptor, {'id': comp.fileName, isProd: false}
                ).bindings || null;

                compProps = compProps
                    ? JSON.stringify(Array.isArray(compProps) ? compProps : Object.keys(compProps))
                    : 'null';
            } catch (error) {
                console.log('skip props filtering for ' + compName);
                compProps = 'null';
            }

            // generate page components imports
            if (comp.lazy) {
                let webpackConfig = '';
                if (comp.chunkName) {
                    webpackConfig += `/* webpackChunkName: ${JSON.stringify(comp.chunkName)} */ `;
                }
                compReg = `() => import(${webpackConfig}'./components/${comp.fileName}')`;
            } else {
                compImports += `import ${compName} from './components/${comp.fileName}';\n`;
                compReg = compName;
            }

            // generate page definition
            compDef.push(`{
            name: '${compName}',
            component: ${compReg},
            props: ${compProps},
            pageIdentifier: ${JSON.stringify(comp.pageIdentifier)}
            }`);

            // generate API mocks
            if (comp.mock && comp.mock.length) {
                comp.mock.forEach(el => {
                    const code = el.code || 200;
                    let response = el.response || '';
                    if (el.responseFile) {
                        response = fs.readFileSync(path.join(comp.fileDir, el.responseFile), 'utf-8');
                    }
                    let url = el.urlRegexp || el.url;
                    let method = 'Any';
                    ['POST', 'GET', 'PATCH', 'PUT', 'DELETE', 'ANY'].forEach((validMethod) => {
                        if (url.startsWith(validMethod + ' ')) {
                            method = validMethod.charAt(0).toUpperCase() + validMethod.slice(1).toLowerCase();
                            url = url.slice(validMethod.length + 1);
                        }
                    });

                    let mockOnParams = '';
                    if (el.params) {
                        mockOnParams += ', ' + JSON.stringify(el.params);
                    }

                    url = el.urlRegexp ? `new RegExp(${JSON.stringify(url)})` : `"${url}"`;
                    mockDef += `mock.on${method}(${url}${mockOnParams}).reply(${code}, ${JSON.stringify(response)});\n`;
                    // mock.onGet("/api/address_lines").reply(200,
                    //     [{text: 'line1'}, {text: 'line2'}]
                    // );
                });
            }
        })

        return compImports + mockDef + '\n\nexport default [\n' + compDef.join(`,\n`) + '\n];';
    }

    fileNameToComponentName(fileName) {
        // in case it's path
        if (fileName.includes('/')) {
            fileName = fileName.substring(fileName.lastIndexOf('/') + 1).replace('.vue', '');
        }

        const compName = fileName
            .replace(/[\-\.]/g, ' ')
            .replace(/(?:^\w|[A-Z]|\b\w)/g, (word, index) => {
                return index === 0 ? word.toLowerCase() : word.toUpperCase();
            })
            .replace(/\s+/g, '');

        return compName[0].toUpperCase() + compName.slice(1);
    }

    generateScriptIfNeeded(componentString, config, fileName) {
        // Parse file content
        let component = VueCompiler.parse(componentString).descriptor;
        if (component.script) {
            return componentString;
        }
        if (!component.template) {
            componentString = '<template>' + componentString + '</template>';
        }

        const compName = this.fileNameToComponentName(fileName);
        const props = config && config.context ? Object.keys(config.context) : [];

        return componentString + `\n<script>export default {name:"${compName}",props:${JSON.stringify(props)}}</script>`;
    }

    uniqueComponentFileName(rootDir, fileName) {
        let exportPath = path.join(rootDir, `${fileName}.vue`);

        for (let i = 2; i < 100; i++) {
            if (!fs.existsSync(fileName)) {
                return fileName;
            }
            fileName = fileName + i;
            exportPath = path.join(rootDir, `${fileName}.vue`);
        }
    }
}

module.exports = new VueFilesHelper;
