'use strict';

const path = require('path');
const fs = require('fs');
const FilesystemHelper = require('../helper/FilesystemHelper');
const VueFilesHelper = require('../helper/VueFilesHelper');

const FRATOMIC_SRC_PATH = 'src/alivue';
const COMPONENTS_PATH = FRATOMIC_SRC_PATH + '/components';

class BuildFratomic {
	buildComponents(outputRootPath, components, forFratomic) {
		this.clearComponentFiles(outputRootPath);
		const componentRootPath = path.join(outputRootPath, COMPONENTS_PATH);

		const items = components.flattenDeep().toArray();
		const routes = [];
		const pageComponents = [];

		// export vue components and collect routes
		let withMock = false;
		let registered = 0;
		for (const item of items) {
			const configData = item.component().configData;
			let alivueConfig = configData && configData.alivue || null;

			if (
				!forFratomic && (
					alivueConfig && alivueConfig.exclude
					|| (item.config && item.config.alivue && item.config.alivue.exclude) // variation exclude
				)
			) {
				continue;
			}

			const exportedFileName = VueFilesHelper.uniqueComponentFileName(
				componentRootPath,
				item.alias || item.handle
			);

			// build fratomic component to vue
			let componentContent = VueFilesHelper.fixImport(item.getContentSync());
			componentContent = VueFilesHelper.fixStyleInclude(componentContent);
			componentContent = VueFilesHelper.generateScriptIfNeeded(componentContent, configData, exportedFileName);
			fs.writeFileSync(path.join(componentRootPath, `${exportedFileName}.vue`), componentContent);

			if (forFratomic) {
				alivueConfig = alivueConfig || {};
				alivueConfig.pageIdentifier = 'Component' + VueFilesHelper.fileNameToComponentName(exportedFileName);
				alivueConfig.lazy = false;
			}

			// generate page route or component
			if (alivueConfig) {
				// common config
				const commonConfig = {
					lazy: alivueConfig.lazy !== false, // true by default
					chunkName: alivueConfig.chunkName || null,
					mock: alivueConfig.mock || null,
				};

				// add route
				if (alivueConfig.routing) {
					routes.push(Object.assign({
						config: alivueConfig.routing,
						fileName: exportedFileName
					}, commonConfig));
				}

				// add dynamic page (CMS route)
				if (alivueConfig.pageIdentifier) {
					pageComponents.push(Object.assign({
						fileDir: item.viewDir,
						fileName: exportedFileName,
						pageIdentifier: Array.isArray(alivueConfig.pageIdentifier)
							? alivueConfig.pageIdentifier : [alivueConfig.pageIdentifier],
					}, commonConfig));
				}

				if (alivueConfig.mock) {
					withMock = true;
				}
			}
			++registered;
		}
		console.log(`Register ${registered} components`);

		// we need empty component for vue routing when CMS routing rendered
		fs.writeFileSync(
			path.join(outputRootPath, COMPONENTS_PATH, `empty.vue`),
			VueFilesHelper.emptyComponent()
		);

		console.log(`Generate ${routes.length} route` + (routes.length !== 1 ? 's' : ''));
		fs.writeFileSync(
			path.join(outputRootPath, FRATOMIC_SRC_PATH, 'routes.js'),
			VueFilesHelper.generateRoutesDefinitionFile(routes)
		);

		console.log(`Generate ${pageComponents.length} page` + (pageComponents.length !== 1 ? 's' : ''));
		withMock = withMock && forFratomic;
		fs.writeFileSync(
			path.join(outputRootPath, FRATOMIC_SRC_PATH, 'dynamicPageComponents.js'),
			VueFilesHelper.generateDynamicPageComponents(
				pageComponents, componentRootPath, withMock
			)
		);
	}

	clearComponentFiles(outputPath) {
		const componentsPath = path.join(outputPath, COMPONENTS_PATH);

		FilesystemHelper.createDirectory(path.join(outputPath, FRATOMIC_SRC_PATH));
		FilesystemHelper.createDirectory(componentsPath);
		// clear previously built components
		FilesystemHelper.deleteFolderRecursive(componentsPath, false);
	}

}

module.exports = BuildFratomic;
