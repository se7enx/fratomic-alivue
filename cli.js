#!/usr/bin/env node

const Boilerplate = require('./src/service/Boilerplate');

(new Boilerplate())
    .boilerplate(process.cwd())
