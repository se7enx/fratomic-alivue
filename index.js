'use strict';

module.exports = {
	FractalVueAdapter: require('./src/fractal-adapter'),
	AlivueCli: require('./src/alivue-cli'),
};
