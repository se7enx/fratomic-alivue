# Fratomic Alivue

Use Fratomic Alive Vue project to breathe life into your atoms and organisms.

Allows to build live single page website that uses Vue 3 components and API data.
Optionally, can be deployed with Server-Side Rendering.

# Installation

1. Make sure you have [Vue CLI](https://cli.vuejs.org/guide/installation.html) 
and [Fratomic CLI](https://github.com/ninety-six/fratomic#installing) installed.

2. Open empty directory of your project, and run:
```shell script
npm install --save @contextualcode/fratomic-alivue
npx alivue-init
npm install
```
> This should be one-time action, feel free to edit generated files.
> In most cases, you'll need to update App.vue component and add header, footer, etc.

3. Update `fractal.config.js` file to contain details about your project.

# Usage

- You can use full vue file definition, as in ["address.vue"](boilerplate/prototype/01-atoms/02-address/address.vue).
Or, you can keep it short with template only, as in ["button.vue"](boilerplate/prototype/01-atoms/01-buttons/button.vue).
For shortened version, `fratomic-alivue` will generate script tag automatically,
based on ["*.config.json"](boilerplate/prototype/01-atoms/01-buttons/button.config.json) context.

- To import components or modules outside of prototype, use `@` alias.
```js
import RequestHandler from '@/handler/RequestHandler';
```

- Update your fratomic components (in `prototype` folder) configs to contain `alivue` configs where needed.
```json
{
    "alivue": {
        "pageIdentifier": "frontpage",
        "pageIdentifier": ["frontpage", "folder"],
        "lazy": false,
        "chunkName": "search",
        "exclude": true,
        "routing": {
            "route": "/searchRequired",
            "name": "optionalName",
            "metaTitle": "(optional) Search page - Project Name",
            "metaKeywords": "(optional) find, search, look for result",
            "metaDescription": "(optional) This page allows search by keyword",
            "metaImageUrl": "(optional) https://somedomain.local/images/image.png"
        },
        "mock": [
            {
                "url": "GET /api/address_lines",
                "code": 200,
                "response": [{"text": "line 1"}, {"text": "line 2"}]
            },
            {
                "urlRegexp": "GET ^/api/address.*",
                "responseFile": "./api_response.json"
            },
            {
                "url": "POST /api/search",
                "params": {"query": "Test"},
                "code": 404,
                "response": {"searchItems": []}
            },
            {
                "url": "POST /api/search",
                "code": 200,
                "params": {"query": "ok"},
                "response": {"searchItems": [{"company": "ContextualCode"}]}
            }
        ],
        "context": {
            "_store": {
                "PageStore": {
                    "pageProps": {},
                    "pageMeta": {},
                    "pageComponentName": "folder"
                },
                "UserStore": {
                    "role": "admin",
                    "isLoggedIn": true
                }
            }
        }
    }
}
```
> `pageIdentifier` can be array or string.
> Defines which page component use for an API response.

> `routing` needed only if you want to display it without API URL.

> `lazy` (default: `true`). Applicable only for pages and routes.
> `true` - loaded on demand. `false` - bundled with main app script.

> `exclude` (default: `false`). If `true` - component excluded from build to live project.

> `chunkName` (default: `null`). Applicable only for lazy pages and routes.
> `null` - unique chunk name generated, so bundled separately from others. 
> `"some-string"` - bundled into named chunk, useful when components placed on the same page.

> All parameters optional, and you can skip `"alivue"` too.

> `mock` config implemented using [axios-mock-adapter](https://www.npmjs.com/package/axios-mock-adapter).
> Check it to see how options are used.
> Make sure you're using `@/handler/RequestHandler` axios instance for your API communication.
> Defaults - method: ANY, code: 200, response: "", params: {}
>
> `mock[0].url` - accepts method + URL or URL (ANY method will be used). Methods: GET, POST, PATCH, PUT, ANY
>
> `mock[0].urlRegexp` - same as url, but accepts regular expression to match URL
>
> `mock[0].code` - response code
>
> `mock[0].params` - Mocking a request with specific parameters. When using params, you must match all key/value pairs passed to that option.
>
> `mock[0].response` - API response
>
> `mock[0].responseFile` - file to load API response from, ralative to prototype component

> `context._store` config designed to preconfigure Vuex state for component preview.

- To preconfigure Vuex state globally for all components, use `store` field in `prototype/alivue.config.json` file.
For example:
```json
{
    "store": {
        "UserStore": {
            "user": {
                "first_name": "Ivan",
                "last_name": "Ternovtsiy"
            },
            "userLoaded": true
        }
    }
}
```
> local `context._store` can be used to override globals.

- Build client and server side dist
```shell script
npm run build
```

- Start fractal in sync mode, which will pick up updates in prototype files
```shell script
npm run fractal-start-sync
```
> Use this instead of `fractal start --sync` to make sure Vue files rebuilt too. 

- Update configuration in `path/to/live-vue-project-name/.env`.
> Tip: You can use `.env.local` for local override.

- Configure links to ignore in `src/handler/PageHandler.js` if needed.
> We ignore links to pdf and image files by default.

- By default, page data loaded from `/api/${window.location.href}` and expects structure like this:
```json
{
    "type": "page type defined in `pageIdentifier`",
    "data": {
        "attribute name used as prop for the Component": "value",
        "metaTitle": "",
        "metaKeywords": "",
        "metaDescription": "",
        "metaImageUrl": ""
    }
}
```
> Fields `metaTitle`, `metaKeywords`, `metaDescription`, `metaImageUrl` used to build page head tags,
> and removed from props by default.

> If you prefer other API structure for component props or meta, you can customize it in `src/handler/PageHandler.js`.

- Build and start fractal project
```shell script
npm run fractal-start
# or, start fractal in sync mode
npm run fractal-start-sync
```

- To build static version, use `npm run build-static` instead of `fractal build`
> this will take care about proper `index.html` file creating

# Usage without Server side rendering and with fallback to client-side rendering.

- If you don't use SSR, update your `src/main.js` file to use `createApp` instead of `createSSRApp`.
> this will reduce final JS file size and exclude client-side hydration code.

- If you're using SSR and client-side as a fallback (while SSR is not available or down),
  update your `public/index.html` file.
  From `<div id="app"></div>` to `<div id="app"><div style="display:none"></div></div>`.
> Otherwise, you'll see `Cannot read property nodeType of null` error in console while using
> createSSRApp without SSR content.

# Styles & assets

- Use `~@/` folder alias to include relative image in scss.
```css
body {
    background-image: url('~@/assets/logo.png');
}
```

- You can use both alias and relative import for scss files
```scss
@import "./imported";
@import "~@/assets/scss/imported";
```

- Be aware we have configured PurgeCSS plugin.
Check how it's configured in [boilerplate/vue.config.js](boilerplate/vue.config.js)
and check [https://purgecss.com/configuration.html#configuration-file](https://purgecss.com/configuration.html#configuration-file) 
for more configuration options. You may need to add some css rules to the `safelist` option.

# Docker usage

- Update `image: registry.gitlab.com/contextualcode/project/express` in `docker/docker-compose.yml`

- Use `express` for SSR version (recommended), or `nginx` to serve without SSR
```shell script
cd docker
docker-compose build express
docker push registry.gitlab.com/contextualcode/project/express

docker-compose up express
# or:
docker run -d -p 80:3000 registry.gitlab.com/contextualcode/project/express
```
