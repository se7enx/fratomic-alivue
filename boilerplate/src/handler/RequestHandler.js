export default new class RequestHandler {
    constructor() {
        const axios = require('axios');

        this.axiosInstance = axios.create({
            responseType: 'json',
            headers: {'X-Requested-With': 'XMLHttpRequest'}
        });

        this.apiPrefix = process.env.VUE_ENV === 'server'
            ? process.env.VUE_APP_API_PREFIX_SERVER
            : process.env.VUE_APP_API_PREFIX_CLIENT;
    }

    loadPage(pageUrl) {
        return this.doGetRequest(this.apiPrefix + pageUrl);
    }

    doGetRequest(path) {
        return this.axiosInstance.get(path).then((response) => {
            return response.data || {};
        });
    }
}
