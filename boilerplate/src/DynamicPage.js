import { h, onMounted } from 'vue';
import { useRouter } from 'vue-router';
import { useStore } from 'vuex';
import PageHandler from './handler/PageHandler';

export default {
    name: 'app-dynamic-page',
    computed: {
        ViewComponent() {
            return useStore().getters.currentPageComponent || 'script';
        },
        ViewComponentProps() {
            return useStore().getters.currentPageProps || {};
        }
    },
    render() {
        return h(this.ViewComponent, this.ViewComponentProps)
    },
    setup() {
        onMounted(() => {
            const route = useRouter().currentRoute.value;
            const store = useStore();

            if (route.name === 'PageEmpty' && !store.getters.currentPageComponentName) {
                store.dispatch('fetchPage', {url: route.fullPath})
                    .then(response => PageHandler.updatePageHead(response, store));
            }
        });
    },
    watch: {
        $route: function (toRoute) {
            if (toRoute.name === 'PageEmpty') {
                this.$store.dispatch('fetchPage', {
                    url: toRoute.fullPath,
                    redirectOnFail: PageHandler.navigatedFromLink
                }).then(response => PageHandler.updatePageHead(response, this.$store));
            } else {
                PageHandler.updatePageHeadByRoute(toRoute, this.$store);
                this.$store.dispatch('resetPage');
            }
            PageHandler.navigatedFromLink = false;
        }
    },
    serverPrefetch() {
        const route = this.$router.currentRoute.value;
        if (route.name === 'PageEmpty' && !this.$store.getters.currentPageComponentName) {
            return this.$store.dispatch('fetchPage', {url: route.fullPath})
                .then(response => PageHandler.updatePageHead(response, this.$store));
        } else {
            PageHandler.updatePageHeadByRoute(route, this.$store);
        }
    },
}
