import { createSSRApp } from 'vue';
import { createConfiguredRouter } from './router';
import { createConfiguredStore } from './store';
import { createWebHistory } from "vue-router";
import App from './App.vue';

// create router and store instances
const router = createConfiguredRouter(
    createWebHistory(process.env.BASE_URL)
);
const store = createConfiguredStore();

if (window.__INITIAL_STATE__) {
    // We initialize the store state with the data injected from the server
    store.replaceState(window.__INITIAL_STATE__);
}

// Note that to enable hydration on the client, use createSSRApp instead of createApp to create your app instance
// (separate imports allows hydration logic to be tree-shakeable for apps that do not require hydration).
const app = createSSRApp(App)
    .use(store)
    .use(router);

router.isReady().then(() => {
    app.mount('#app');
});
