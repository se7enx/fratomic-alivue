import RequestHandler from '../handler/RequestHandler';
import PageHandler from '../handler/PageHandler';
import pageComponents from "../alivue/dynamicPageComponents";

export const createPageStore = () => {
    return {
        state: {
            pageProps: {},
            pageMeta: {},
            pageComponentName: null,
        },
        actions: {
            resetPage(context) {
                return context.commit('resetPage');
            },
            setPageByComponentName(context, componentDetails) {
                return context.commit('setPageByComponentName', componentDetails);
            },
            setPageMeta(context, pageMeta) {
                return context.commit('setPageMeta', pageMeta);
            },
            fetchPage(context, request) {
                return RequestHandler.loadPage(request.url)
                    .then(response => {
                        context.commit('setPage', response);

                        return response;
                    })
                    .catch(error => {
                        if (request.redirectOnFail && process.env.VUE_ENV !== 'server') {
                            window.location.replace(request.url);
                        }

                        return error;
                    })
            },
        },
        mutations: {
            setPage(state, response) {
                const {pageIdentifier, pageProps} = PageHandler.parsePageResponse(response);
                let newPageComponentName = null;

                for (let i = 0; i < pageComponents.length; ++i) {
                    if (pageComponents[i].pageIdentifier.includes(pageIdentifier)) {
                        newPageComponentName = pageComponents[i].name;

                        for (let prop in pageProps) {
                            if (!pageComponents[i].props.includes(prop)) {
                                delete pageProps[prop];
                            }
                        }
                    }
                }

                state.pageComponentName = newPageComponentName;
                state.pageProps = pageProps;
            },
            setPageByComponentName(state, componentDetails) {
                let pageComponentName = null;

                for (let i = 0; i < pageComponents.length; ++i) {
                    if (pageComponents[i].name === componentDetails.name) {
                        pageComponentName = componentDetails.name;

                        for (let prop in componentDetails.props) {
                            if (!pageComponents[i].props.includes(prop)) {
                                delete componentDetails.props[prop];
                            }
                        }
                        break;
                    }
                }

                state.pageComponentName = pageComponentName;
                state.pageProps = componentDetails.props || {};
            },
            resetPage(state) {
                state.pageProps = {};
                state.pageComponentName = null;
            },
            setPageMeta(state, meta) {
                state.pageMeta = meta;
            },
        },
        getters: {
            currentPageProps: state => {
                return state.pageProps;
            },
            currentPageMeta(state) {
                return state.pageMeta;
            },
            currentPageComponentName: state => {
                return state.pageComponentName;
            },
            currentPageComponent: state => {
                if (!state.pageComponentName) {
                    return null;
                }

                for (let i = 0; i < pageComponents.length; ++i) {
                    if (pageComponents[i].name === state.pageComponentName) {
                        return pageComponents[i].component;
                    }
                }

                return null;
            },
        },
    };
};
