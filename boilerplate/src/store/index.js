import { createStore } from 'vuex';
import { createPageStore } from './PageStore';

export const createConfiguredStore = () => {
    const PageStore = createPageStore();

    return createStore({
        state: {},
        mutations: {},
        actions: {},
        modules: {PageStore}
    });
};
