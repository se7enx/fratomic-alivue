const nodeExternals = require('webpack-node-externals');
const ManifestPlugin = require('webpack-manifest-plugin');
const PurgeCSSPlugin = require('purgecss-webpack-plugin');
const {DefinePlugin, optimize} = require('webpack');
const path = require('path');
const glob = require('glob');

const configureClient = (webpack) => {
    webpack.plugin('define-env').use(new DefinePlugin({"process.env.VUE_ENV": "'client'"}));

    // remove unused CSS
    webpack.plugin('purge-css').use(new PurgeCSSPlugin({
        paths: glob
            .sync(`${path.join(__dirname, 'src')}/**/*`, {nodir: true})
            .filter(f => f.endsWith('.vue') || f.endsWith('.html') || f.endsWith('.ts') || f.endsWith('.js')),
        defaultExtractor: function (content) {
            const contentWithoutStyleBlocks = content.replace(/<style[^]+?<\/style>/gi, '');
            return contentWithoutStyleBlocks.match(/[A-Za-z0-9-_/:]*[A-Za-z0-9-_/]+/g) || [];
        },
        fontFace: true,
        safelist: {
            standard: [
                /-(leave|enter|appear)(|-(to|from|active))$/,
                /^(?!(|.*?:)cursor-move).+-move$/,
                /^router-link(|-exact)-active$/, /data-v-.*/
            ],
            deep: [],
            greedy: []
        },
    }));
}

const configureServer = (webpack) => {
    webpack.plugin('define-env').use(new DefinePlugin({"process.env.VUE_ENV": "'server'"}));

    webpack.target('node');
    webpack.externals(nodeExternals({allowlist: /\.(css|vue)$/,}))
    webpack.output.libraryTarget('commonjs-module');
    webpack.plugin('manifest').use(
        new ManifestPlugin({fileName: 'ssr-manifest.json'})
    );
    webpack.plugin('limit-chunk').use(
        new optimize.LimitChunkCountPlugin({maxChunks: 1})
    );

    webpack.optimization.splitChunks(false).minimize(false);

    webpack.plugins.delete('hmr');
    webpack.plugins.delete('preload');
    webpack.plugins.delete('prefetch');
    webpack.plugins.delete('progress');
    webpack.plugins.delete('friendly-errors');

    webpack.entry('app').clear()
        .add('./src/main.server.js');
}

exports.chainWebpack = (webpack) => {
    if (process.env.VUE_ENV !== 'server') {
        configureClient(webpack);
    } else {
        configureServer(webpack);
    }
}
exports.css = {extract: true}; // required for PurgeCSS to work properly in dev mode
exports.outputDir = path.join('./dist', process.env.VUE_ENV !== 'server' ? 'client' : 'server');
